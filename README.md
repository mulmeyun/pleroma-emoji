# pleroma emoji

Custom emojis for my instance godspeed.moe

By default there's a file emoji.txt in /pleroma/config that you need to delete to list all emojis not explicitly defined there.

You can get a list of all emojis available in an instance on https://emojos.in/godspeed.moe
